package com.nab.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CallBackRequest extends BasePage {

	@FindBy(css = "input[label='First name']")
	private WebElement firstNameField;

	@FindBy(css = "input[id='field-page-Page1-aboutYou-lastName']")
	private WebElement lastNameField;

	@FindBy(css = "input[label='Phone number']")
	private WebElement phoneNumberField;

	@FindBy(css = "input[label='Email']")
	private WebElement emailField;

	@FindBy(css = "button[type='submit']")
	private WebElement submit;

	public CallBackRequest(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public void setFirstNameField(String firstName) {
		waitForElementPresent(firstNameField, 20);
		firstNameField.sendKeys(firstName);
	}

	public void setLastNameField(String lastName) {
		waitForElementPresent(lastNameField, 5);
		lastNameField.sendKeys(lastName);
	}

	public void setPhoneNumberField(String phoneNumber) {
		phoneNumberField.sendKeys(phoneNumber);
	}

	public void setEmailField(String email) {
		emailField.sendKeys(email);
	}

	public void selectAreYouNABCust(String val) {
		WebElement ele = driver.findElement(
				By.xpath("//*[contains(text(),'customer?')]/following::input[@value='" + val + "']"));
		clickUsingJavscript(ele);

	}

	public void selectState(String state) {
		WebElement selectStatePlaceHolder = driver
				.findElement(By.xpath("//div[contains(@class,'react-select__placeholder')]"));
		waitForElementPresent(selectStatePlaceHolder, 15);
		selectStatePlaceHolder.click();
		WebElement selectState = driver
				.findElement(By.xpath("//div[contains(@class,'react-select__placeholder') and text()='NSW']"));
		clickUsingActionClass(selectState);

	}

	public void submit() {
		submit.click();
	}

}
