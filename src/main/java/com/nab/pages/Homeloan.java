package com.nab.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Homeloan extends BasePage {

	@FindBy(xpath = "//*[text()='Request a call back']/parent::a")
	private WebElement requestCallBackLink;

	@FindBy(xpath = "//div[@class='button-container green small-btn ']//a")
	private WebElement generalHomeLoanEnquiryLink;
	
	@FindBy(css = "[data-target='home-loans']")
	private WebElement homeloanLink;

	public Homeloan(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public void makeRequestCallBack() {
		requestCallBackLink.click();
	}

	public ContactUs clickEnquireNowButton(String productType)// Basic Variable
	{
		 WebElement ele=driver.findElement(By.xpath("//a[text()='" + productType
				+ "']/ancestor::div[@class='product-card']//a[starts-with(@aria-label,'Enquire now')]"));
		 waitForElementToBeClickable(ele, 20);
		 ele.click();
		 return getPageInstance(ContactUs.class);
	}
	

	public void selectHomeloanLink() {
		homeloanLink.click();
	}


	

}
