package com.nab.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage extends Page {

	public BasePage(WebDriver driver) {
		super(driver);
	}

	@Override
	public String getPageTitle() {
		return driver.getTitle();
	}

	@Override
	public WebElement getWebELement(By locator) {
		try {
			return driver.findElement(locator);
		} catch (Exception e) {
			System.out.println("exception occured while creating a object:" + locator.toString());
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public void waitForElementPresent(WebElement element, int time) {

		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void waitForElementToBeClickable(WebElement element, int time) {

		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	@Override
	public void waitForPageTitle(String title) {
		wait.until(ExpectedConditions.titleContains(title));
	}

	private static WebElement getShadowRoot(WebDriver driver, WebElement shadowHost) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return (WebElement) js.executeScript("return arguments[0].shadowRoot", shadowHost);
	}

	public static WebElement getShadowElement(WebElement shadowHost, String cssOfShadowElement) {
		WebElement shardowRoot = getShadowRoot(driver, shadowHost);
		return shardowRoot.findElement(By.cssSelector(cssOfShadowElement));
	}

	public void clickUsingJavscript(WebElement ele) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
	}

	public void clickUsingActionClass(WebElement ele) {
		Actions act = new Actions(driver);
		act.moveToElement(ele).click().build().perform();
	}

}
