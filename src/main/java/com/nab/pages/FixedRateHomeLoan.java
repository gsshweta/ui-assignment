package com.nab.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FixedRateHomeLoan extends BasePage {

	@FindBy(css = "a[aria-label='Enquire now']")
	private WebElement enquiryNowLink;

	
	public FixedRateHomeLoan(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public ContactUs clickEnquiryNowLink()
	{
		waitForElementToBeClickable(enquiryNowLink, 10);
		enquiryNowLink.click();
		return getPageInstance(ContactUs.class);
	}
}
