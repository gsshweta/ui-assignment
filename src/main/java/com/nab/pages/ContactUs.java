package com.nab.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUs extends BasePage {

//	@FindBy(linkText = "Call me back")
	@FindBy(css = "a[aria-label='Call me back']")
	private WebElement callMeBackLink;

	public ContactUs(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public CustomerAssitanceDirectory clickCallMeBack() {
		waitForElementToBeClickable(callMeBackLink, 20);
		clickUsingJavscript(callMeBackLink);
//		clickUsingActionClass(callMeBackLink);
//		callMeBackLink.click();
		return getPageInstance(CustomerAssitanceDirectory.class);
	}

}
