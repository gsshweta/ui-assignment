package com.nab.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

	@FindBy(xpath = "//a[@class='menu-trigger' and text()='Personal']")
	private WebElement personalMenuLink;

	@FindBy(xpath = "//span[text()='Home loans']/parent::a")
//			"//*[text()='Home loans']/parent::a[@class='menu-trigger']")
	private WebElement homeloanMenuLink;

	@FindBy(xpath = "//*[@class='nav-container']//span[text()='Home loans']/parent::a")
	private WebElement submenuHomeloanLink;

	@FindBy(xpath = "//span[text()='Fixed rate home loan']/parent::a")
	private WebElement fixedRatehomeLoanLink;

	public HomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public void goToPersonalMenu() {
		waitForElementToBeClickable(personalMenuLink, 20);
		personalMenuLink.click();
	}

	public WebElement getMenuLink(String linkText) {
		return driver.findElement(By.xpath("//*[@class='nav-container']//span[text()='" + linkText + "']/parent::a"));
	}

	public void clickHomeloan() {
//		getMenuLink("Home loan").click();
		waitForElementToBeClickable(homeloanMenuLink, 20);
		homeloanMenuLink.click();
	}

	public Homeloan clickSubmenuHomeLoanLink() {
		waitForElementToBeClickable(submenuHomeloanLink, 20);
		submenuHomeloanLink.click();
		return getPageInstance(Homeloan.class);
	}

	public FixedRateHomeLoan clickFixedRateHomeLoanLink() {
		fixedRatehomeLoanLink.click();
		return getPageInstance(FixedRateHomeLoan.class);

	}

}
