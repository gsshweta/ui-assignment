package com.nab.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CustomerAssitanceDirectory extends BasePage {

	public CustomerAssitanceDirectory(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void SelctRadioButton() {

		WebElement shadowHost = driver.findElement(By.cssSelector("#contact-form-shadow-root"));
		waitForElementPresent(shadowHost, 10);
		getShadowElement(shadowHost, "#myRadioButton-0 > label > span").click();
	}

	public CallBackRequest clickNext() {

		WebElement shadowHost = driver.findElement(By.cssSelector("#contact-form-shadow-root"));
		WebElement ele = getShadowElement(shadowHost, "div.sc-bdVaJa.iAQrVS > button > div > span");
		clickUsingJavscript(ele);
		String winHandleBefore = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		return getPageInstance(CallBackRequest.class);

	}

}
