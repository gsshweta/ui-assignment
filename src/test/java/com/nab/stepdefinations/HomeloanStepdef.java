package com.nab.stepdefinations;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.nab.pages.BasePage;
import com.nab.pages.CallBackRequest;
import com.nab.pages.ContactUs;
import com.nab.pages.CustomerAssitanceDirectory;
import com.nab.pages.FixedRateHomeLoan;
import com.nab.pages.HomePage;
import com.nab.pages.Homeloan;
import com.nab.pages.Page;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class HomeloanStepdef {

	WebDriver driver;
	HomePage homePage;
	Page page;
	Homeloan homeloan;
	FixedRateHomeLoan fixedRateHomeLoan;
	ContactUs contactUs;
	CallBackRequest callBackRequest;

	@Before
	public void setup() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://www.nab.com.au/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

		page = new BasePage(driver);

	}

	@After
	public void tearDown() {
		driver.quit();

	}

	@Given("user is on NAB website")
	public void user_is_on_NAB_website() {
		homePage = page.getPageInstance(HomePage.class);

	}

	@And("user navigates home loan")
	public void user_navigates_home_loan() {
		homePage.goToPersonalMenu();
		homePage.clickHomeloan();
//		homeloan = homePage.clickSubmenuHomeLoanLink();
		fixedRateHomeLoan = homePage.clickFixedRateHomeLoanLink();

	}

	@And("user makes enquiry about home loan")
	public void user_makes_enquiry_about_home_loan() {
		contactUs = fixedRateHomeLoan.clickEnquiryNowLink();
	}

	@When("user click call me back link")
	public void user_click_call_me_back_link() {
		CustomerAssitanceDirectory customerAssitanceDirectory = contactUs.clickCallMeBack();
		customerAssitanceDirectory.SelctRadioButton();
		callBackRequest = customerAssitanceDirectory.clickNext();

	}

	@When("fills the details {string} {string} {string} {string} {string}  {string}")
	public void fills_the_details(String firstName, String lastName, String email, String NAB_customer,
			String phoneNumber, String state) {
		callBackRequest.setFirstNameField(firstName);
		callBackRequest.setLastNameField(lastName);
		callBackRequest.selectAreYouNABCust(NAB_customer);
		callBackRequest.setPhoneNumberField(phoneNumber);
		callBackRequest.setEmailField(email);
		callBackRequest.selectState(state);

	}

	@Then("submits the form")
	public void submits_the_form() {
		callBackRequest.submit();
	}

}
