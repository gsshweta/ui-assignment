Feature: Home loan Enquiry
  As a user I should able to enquiry about  homeloan from website NAB website

  Scenario Outline: Enquiry about home loan and request for call back
    Given user is on NAB website
    And user navigates home loan
    And user makes enquiry about home loan
    When user click call me back link
    And fills the details "<firstname>" "<lastname>" "<email>" "<NAB_customer>" "<phoneNumber>"  "<state>"
    Then submits the form

    Examples: 
      | firstname | lastname | email        | NAB_customer | phoneNumber | state |
      | test      | tina     | test@est.com | false        |  0478455899 | NSW   |
